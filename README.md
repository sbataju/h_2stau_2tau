How to make original Direct StauStau decay EVENT file.
Needs 
    MC15JobOpts-00-11-50_v7.tar.gz
    MC15JobOptions/MC15.397034.MGPy8EG_A14N23LO_StauStau_360p0_160p0_TFilt.py
    

asetup MCProd,19.2.5.33.3,here
/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.5/MCProd/19.2.5.33.3/InstallArea/share/bin/Generate_tf.py --ecmEnergy=13000 --evgenJobOpts=MC15JobOpts-00-11-50_v7.tar.gz --firstEvent=2001 --jobConfig=MC15JobOptions/MC15.397034.MGPy8EG_A14N23LO_StauStau_360p0_160p0_TFilt.py --maxEvents=2000 --outputEVNTFile=EVNT._000001.pool.root.1 --randomSeed=2 --runNumber=397034 --skipEvents=0
