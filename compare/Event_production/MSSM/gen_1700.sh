#!/bin/bash
#PBS -q hep
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
cd /cluster/home/bataju/BENCHMARK/1700/event
asetup 19.2.5.21.3,MCProd,here
Generate_tf.py --ecmEnergy=13000. --maxEvents=20000 --runNumber=9999999 --firstEvent=1 --randomSeed=123456 --outputEVNTFile=EVNT.root --jobConfig=MC15.9999999.MadGraphPythia8EvtGen_BmS_H_staustau_1700.py
