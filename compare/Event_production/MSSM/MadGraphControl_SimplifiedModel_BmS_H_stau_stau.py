from MadGraphControl.MadGraphUtils import *
import re
import os 
#following examples from:
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#LO_Pythia8_Showering_Inclusive_s
#asetup 19.2.5.21.3,MCProd,here
#Generate_tf.py --ecmEnergy=13000. --maxEvents=-1 --runNumber=9999999 --firstEvent=1 --randomSeed=123456 --outputEVNTFile=EVNT.root --jobConfig= 

def StringToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)

mode=0

evt_multiplier = 2.0
if evt_multiplier > 0:
  if runArgs.maxEvents > 0:
    nevts = runArgs.maxEvents * evt_multiplier
  else:
    nevts = 5000 * evt_multiplier



filename = runArgs.jobConfig[0][:-3] # cut off trailing .py
# mstau      = StringToFloat(filename.split('_')[3]) 
mstau    = 350 # mass of stau
mhiggs   = StringToFloat(filename.split('_')[4]) # mass of higgs

print "mass of higgs set to :", mhiggs
print "mass of stau set to: ", mstau


fcard = open('proc_card_mg5.dat','w')
fcard.write('''
set group_subprocesses Auto
set ignore_six_quark_processes False
set gauge unitary
set complex_mass_scheme False
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
import model mssm
generate     g g > b b~ h2, ( h2 > ta1+ ta1-, ta1+ > ta+ n1, ta1- > ta- n1 )
add process  g g > b b~ h3, ( h3 > ta1+ ta2-, ta1+ > ta+ n1, ta2- > ta- n1 )
add process  g g > b b~ h3, ( h3 > ta2+ ta1-, ta2+ > ta+ n1, ta1- > ta- n1 )
add process  g g > b b~ h2, ( h2 > ta2+ ta1-, ta2+ > ta+ n1, ta1- > ta- n1 )
add process  g g > b b~ h2, ( h2 > ta1+ ta2-, ta1+ > ta+ n1, ta2- > ta- n1 )
output HAbb_2sta_2ta2n
''')
fcard.close()



beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

extras={'lhe_version' : '2.0',
    'cut_decays'  : 'F',
    'pdlabel'      :"'lhapdf'",
    'lhaid'        :230000,
    'event_norm'   :'sum',
    'use_syst'     :'F'
}
process_dir = new_process()

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',xqcut=0,
               nevts=nevts,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

#changing the higgs and stau masses


evgenConfig.description = 'H->stausatu, mhass,mstau: %s,%s' %(mhiggs,mstau)

if not os.access(process_dir+'/Cards/param_card.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open(process_dir+'/Cards/param_card.dat','r')
    newcard = open('param_card.dat','w')
    count = 0
    for line in oldcard:
      #higgs mass
      if '#  mh02' in line:
        newcard.write('      35 {} #  mh02\n'.format(mhiggs))
      elif '#  ma0' in line:
        newcard.write('      36 {} #  ma0\n'.format(mhiggs))
      elif '#  mh' in line:
        count +=1
        if count == 2:    newcard.write('      37 {} #  mh\n'.format(mhiggs))
        else:   newcard.write(str(line))
      #stau mass  
      elif '#  msl3'  in line:
        newcard.write('      1000015 {} #  msl3\n'.format(mstau))
      elif '#  msl6' in line:
        newcard.write('      2000015 {} #  msl6\n'.format(mstau))
      #M1 mass   
      elif '#  mneu1' in line:
        newcard.write('      1000022 180 #  mneu1\n')
      #M2 mass
      elif '#  mneu2' in line:
        newcard.write('      1000023 300 #  mneu2\n')
      #sbottom
      elif '#  msd3' in line:
        newcard.write('      1000005 1500 #  msd3\n')
      #stop
      elif '#  msu3' in line:
        newcard.write('      1000006 1500 #  msu3\n')
      #msd6
      elif '#  msd6' in line:
        newcard.write('      2000005 1500 #  msd6\n')
      #msu6
      elif '#  msu6' in line:
        newcard.write('      2000006 1500 #  msu6\n')
      #heavy neu 3/4
      elif '#  mneu3' in line:
        newcard.write('      1000025 2500 #  mneu3\n')
      elif '#  mneu4' in line:
        newcard.write('      1000035 2500 #  mneu4\n')
      else:
        newcard.write(str(line))
    oldcard.close()
    newcard.close()


runName='run_01'


generate(run_card_loc='./run_card.dat',param_card_loc='./param_card.dat',mode=mode,run_name=runName,proc_dir=process_dir)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=2,saveProcDir=True)



evgenConfig.description = 'H->stausatu'
evgenConfig.keywords+=['BSM','SUSY']
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")