#!/bin/bash
#PBS -q hep
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
cd /cluster/home/bataju/BENCHMARK/1700/truth
asetup 21.0.53,Athena,here
Reco_tf.py --inputEVNTFile ../event/EVNT.root --outputDAODFile test.pool.root --reductionConf TRUTH1
