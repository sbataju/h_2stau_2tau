#!/bin/bash
top_dir=($PWD)
for dir in {0..20000..1000}; do
        mkdir truth_$dir
done
for dir in {0..20000..1000}; do
        cd $top_dir/truth_$dir
        echo "#!/bin/bash" > event_no_${dir}.sh
        echo "#PBS -q hep" >> event_no_${dir}.sh
        echo "#PBS -l qos=hep " >> event_no_${dir}.sh
        echo "#PBS -l walltime=21:00:00" >> event_no_${dir}.sh
        echo "cd ${PWD}">>  event_no_${dir}.sh
        echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"  >> event_no_${dir}.sh
        echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"  >> event_no_${dir}.sh
        echo "asetup AtlasOffline,21.0.20,here" >> event_no_${dir}.sh
        echo "Reco_tf.py --inputEVNTFile /cluster/home/bataju/directStau/Benchmark/events/1700_gg_withoutweight/EVNT._0.pool.root  --maxEvents=1000 --skipEvents=${dir} --outputDAODFile test.pool.root --reductionConf TRUTH1">> event_no_${dir}.sh
        chmod +x event_no_${dir}.sh
        qsub event_no_${dir}.sh
        cd $top_dir
done

# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
# cd /cluster/home/bataju/directStau/Benchmark/truth/1700/
# asetup 21.0.53,Athena,here
# Reco_tf.py --inputEVNTFile /cluster/home/bataju/directStau/event/1700_1/EVNT._000001.pool.root.1  --outputDAODFile test.pool.root --reductionConf TRUTH1
