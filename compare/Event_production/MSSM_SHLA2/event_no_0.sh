#!/bin/bash
#PBS -q hep
#PBS -l qos=hep 
#PBS -l walltime=12:00:00
cd /cluster/home/bataju/directStau/Benchmark/events/1700_gg_withoutweight/
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup MCProd,19.2.5.33.3,here
Generate_tf.py --AMITag=e6707 --ecmEnergy=13000 --evgenJobOpts=MC15JobOpts-00-11-50_v7.tar.gz --jobConfig=MC15JobOptions/MC15.397034.MGPy8EG_A14N23LO_StauStau_350p0_180p0_1700p0_TFilt.py --runNumber=397034 --randomSeed=2 --firstEvent=0 --maxEvents=20000 --skipEvents=0 --outputEVNTFile=EVNT._0.pool.root
